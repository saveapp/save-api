from django.test import TestCase as DjangoTestCase
from django.contrib.auth import get_user_model
from django.conf import settings


class TestCase(DjangoTestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()

    @staticmethod
    def create_test_user(username='tester', password='s3cur3'):
        return get_user_model().objects.create_user(
            username=username,
            password=password
        )
