# Generated by Django 2.0.5 on 2018-07-10 18:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('saveapi', '0002_link'),
    ]

    operations = [
        migrations.AlterField(
            model_name='link',
            name='annotation',
            field=models.TextField(blank=True),
        ),
    ]
