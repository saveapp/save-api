from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.management.commands.createsuperuser import get_user_model
from django.conf import settings
from django.db.utils import IntegrityError

import logging
logger = logging.getLogger(__name__)


DEFAULT_DATABASENAME = list(settings.DATABASES.keys())[0]


class Command(BaseCommand):
    help = 'Setup an initial superuser without interaction'

    def add_arguments(self, parser):
        parser.add_argument(
            'username',
            type=str,
            help='The username to be set'
        )
        parser.add_argument(
            'password',
            type=str,
            help='The password to be set'
        )
        parser.add_argument(
            'email',
            type=str,
            nargs='?',
            help='The email to be set (optional)'
        )

        parser.add_argument(
            '--dbname',
            type=str,
            default=DEFAULT_DATABASENAME,
            help=f'The database to use. Default: \'{DEFAULT_DATABASENAME}\''
        )


    def handle(self, *args, **options):
        username = options['username']
        password = options['password']
        email = options.get('email', None)
        dbname = options['dbname']

        logger.info(f'Creating initial superuser: {username} ...')
        try:
            get_user_model()._default_manager.db_manager(dbname).create_superuser(
                username=username,
                email=email,
                password=password
            )
        except IntegrityError as e:
            logger.error(f'User already exists: {e}')
        else:
            logger.info(f'Superuser successfully setup')
