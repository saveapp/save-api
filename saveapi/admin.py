from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin

from .models import User, Link

@admin.register(User)
class UserAdmin(AuthUserAdmin):
    pass

@admin.register(Link)
class LinkAdmin(admin.ModelAdmin):
    list_display = ['url', 'annotation', 'created_by']
