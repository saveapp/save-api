from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.db import models
from django.db.utils import IntegrityError
from django.shortcuts import get_object_or_404
from django.http import Http404

from django_prometheus.models import ExportModelOperationsMixin

from logging import getLogger
logger = getLogger(__name__)

from saveapi import utils


class User(ExportModelOperationsMixin('user'), AbstractUser):
    @property
    def name(self):
        return self.username

    def links(self, sort_order=None):
        return Link.objects.filter(created_by=self).order_by(utils.sort_order(sort_order, 'id'))

    def link(self, link_id):
        matching_link = get_object_or_404(Link, pk=link_id)
        if matching_link.created_by != self:
            logger.warning(f'User {self.id} tried accessing foreign link: {link_id}')
            raise Http404

        return matching_link

    def json(self):
        return {
            'id': self.id,
            'name': self.username,
        }


class Link(ExportModelOperationsMixin('link'), models.Model):
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    url = models.URLField(validators=[URLValidator])
    annotation = models.TextField(blank=True)

    def to_json(self):
        return {
            'id': self.id,
            'created_on': str(self.created_on),
            'updated_on': str(self.updated_on),
            'url': self.url,
            'annotation': self.annotation
        }

    @staticmethod
    def from_json(link_json, creating_user):
        link = Link(
            created_by=creating_user,
            url=link_json.get('url', None),
            annotation=link_json.get('annotation', '')
        )

        try:
            link.full_clean()
            link.save()
        except (ValidationError, IntegrityError) as e:
            logger.error(e)
            raise

        return link

    def __str__(self):
        return f'{self.url}'

    def __eq__(self, other):
        if self.url != other.url:
            return False

        if self.annotation != other.annotation:
            return False

        if self.created_by != other.created_by:
            return False

        return True
