def sort_order(order_string, field_name):
    """Generate the sort parameter for queryset.order_by()"""
    if order_string is None:
        order_string = 'asc'

    if order_string.lower() == 'asc':
        prefix = ''
    elif order_string.lower() == 'desc':
        prefix = '-'
    else:
        raise ValueError(f'Invalid sort order: {order_string}')

    return f'{prefix}{field_name}'
