from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from django.http import Http404

from saveapi.test_utils import TestCase
from saveapi.models import Link


class LinkTest(TestCase):
    def setUp(self):
        super().setUp()

        self.tester = self.create_test_user()

    def test_should_create_link_from_valid_json(self):
        link_json = {
            'annotation': 'An annotation',
            'url': 'https://example.com'
        }

        link = Link.from_json(link_json, self.tester)

        self.assertEqual(link, Link(url=link_json['url'], annotation=link_json['annotation'], created_by=self.tester))

    def test_should_create_link_without_annotation(self):
        link_json = {
            'url': 'https://example.com'
        }

        link = Link.from_json(link_json, self.tester)

        self.assertEqual(link, Link(url=link_json['url'], annotation='', created_by=self.tester))

    def test_should_create_link_with_empty_annotation(self):
        link_json = {
            'url': 'https://example.com',
            'annotation': ''
        }

        link = Link.from_json(link_json, self.tester)

        self.assertEqual(link, Link(url=link_json['url'], annotation='', created_by=self.tester))

    def test_should_raise_error_when_url_missing(self):
        invalid_json = {
            'annotation': 'lol where\'s my url?'
        }

        with self.assertRaises(ValidationError):
            Link.from_json(invalid_json, self.tester)

    def test_should_raise_error_for_invalid_json(self):
        invalid_json = {
            'url': 'https://example.com',
            'annotation': None
        }

        with self.assertRaises(IntegrityError):
            Link.from_json(invalid_json, self.tester)

    def test_should_fail_for_invalid_url(self):
        invalid_json = {
            'url': 'lk;jsgadla;gs'
        }

        with self.assertRaises(ValidationError):
            Link.from_json(invalid_json, self.tester)

    def test_should_save_link_when_other_user_has_same_link(self):
        link_json = {
            'annotation': 'An annotation',
            'url': 'https://example.com'
        }
        tester_link = Link.from_json(link_json, self.tester)
        some_user = self.create_test_user('not tester')

        another_link = Link.from_json(link_json, some_user)

        self.assertEqual(tester_link.url, another_link.url)
        self.assertEqual(tester_link.annotation, another_link.annotation)
        self.assertNotEqual(tester_link, another_link)


class UserTest(TestCase):
    def setUp(self):
        super().setUp()

    def test_should_retrieve_all_links_by_user(self):
        user = self.create_test_user(username='tester')
        different_user = self.create_test_user(username='different')
        user_link_1 = Link.from_json({'url': 'https://example.com'}, user)
        user_link_2 = Link.from_json({'url': 'https://example.com/test'}, user)
        user_link_3 = Link.from_json({'url': 'https://example.com/test/another_link'}, user)
        different_user_link = Link.from_json({'url': 'https://example.com'}, different_user)

        result = user.links()
        expected = [user_link_1, user_link_2, user_link_3]

        self.assertEqual(list(result), expected)

    def test_should_retrieve_all_links_by_user_sorted_descending(self):
        user = self.create_test_user(username='tester')
        different_user = self.create_test_user(username='different')
        user_link_1 = Link.from_json({'url': 'https://example.com'}, user)
        user_link_2 = Link.from_json({'url': 'https://example.com/test'}, user)
        user_link_3 = Link.from_json({'url': 'https://example.com/test/another_link'}, user)
        different_user_link = Link.from_json({'url': 'https://example.com'}, different_user)

        result = user.links(sort_order='desc')
        expected = [user_link_3, user_link_2, user_link_1]

        self.assertEqual(list(result), expected)

    def test_should_return_empty_list_without_saved_links(self):
        user = self.create_test_user()

        result = user.links()

        self.assertEqual(list(result), [])

    def test_should_retrieve_specific_link(self):
        user = self.create_test_user()
        link = Link.from_json({'url': 'https://example.com'}, user)

        result = user.link(link.id)

        self.assertEqual(result, link)

    def test_should_raise_404_error_for_nonexisting_link(self):
        user = self.create_test_user()

        with self.assertRaises(Http404):
            user.link(42)

    def test_should_raise_404_error_for_link_of_different_user(self):
        user = self.create_test_user('tester')
        different_user = self.create_test_user('not tester')
        foreign_link = Link.from_json({'url': 'https://example.com'}, different_user)

        with self.assertRaises(Http404):
            user.link(foreign_link.id)
