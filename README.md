# Saveapp API
[![pipeline status](https://gitlab.com/saveapp/save-api/badges/master/pipeline.svg)](https://gitlab.com/saveapp/save-api/commits/master)
[![coverage report](https://gitlab.com/saveapp/save-api/badges/master/coverage.svg)](https://gitlab.com/saveapp/save-api/commits/master)

A Django-based REST API for saving links you want to read later.

> **Note:** This is a from-scratch rewrite of the legacy save API written in Flask.
> There may be some minor API changes introduced when rewriting the project.
> See [differences to v1.0][#differences-to-v10].

## Development
You can do all of this using your system's Python installation, or using a virtual environment so you don't impact your system's Python setup.
If you choose the former, just replace `venv/bin/...` in this readme with your system's executable.

First, create a virtual environment:
```shell
python -m venv venv
```

Install Python requirements:
```shell
venv/bin/pip install -r requirements.txt
```

To setup the Django project, you'll want to setup the database and create a local superuser account:
```shell
# setup database migrations
venv/bin/python manage.py migrate

# setup a local admin account
venv/bin/python manage.py createsuperuser
```

### Local Settings
Setup a `local_settings.py` so you can configure your local dev setup without affecting `settings.py` in the repo.

You can use the `local_settings.py` to override settings from `settings.py`, e.g. set `DEBUG = True` or play around with `ALLOWED_HOSTS` etc.

### Running
Start a dev server:
```shell
venv/bin/python manage.py runserver [<IP>:<PORT>]
```

## Deployment
To deploy your own saveapp API, there's an example docker-compose setup in the `docker/` directory which you can copy and adjust to your needs.  
The latest version of the pre-built docker image is available from this repo's registry: `registry.gitlab.com/saveapp/save-api:latest`

## Tests
```shell
venv/bin/python manage.py test
```

If you want to use a specific log level for that, you can use the `DJANGO_LOG_LEVEL` environment variable:
```shell
DJANGO_LOG_LEVEL="WARNING" venv/bin/python manage.py test
```

## API Access
TBA

### Differences to v1.0
#### Response Codes
- When disabling registration for the API, the registration endpoint now answers with 501 instead of 403.

#### Saving Links
- The endpoint for saving links changed from `/save/api/save` to `/api/links`.
- Saving a link now only accepts one link at a time.
- The response doesn't include the user ID anymore.

## Prometheus Metrics
This project uses [korfuri/django-prometheus][django-prometheus] to export metrics about the Django app for [Prometheus](https://prometheus.io).

You can access them via the `/metrics` endpoint.

> **Note:** Access to the metrics endpoint is not handled by this API.
> If you don't want these metrics to be publicly accessible, you may want to configure your reverse proxy to ask for authentication for `/metrics`.

## Resources
- [encode/django-rest-framework](https://github.com/encode/django-rest-framework)
- [korfuri/django-prometheus][django-prometheus]
- [heroku-python/dj-static](https://github.com/heroku-python/dj-static)


[django-prometheus]: https://github.com/korfuri/django-prometheus
