#!/usr/bin/env bash
python manage.py migrate
python manage.py collectstatic --noinput
python manage.py createinitialsuperuser "${DJANGO_SU_NAME}" "${DJANGO_SU_EMAIL}" "${DJANGO_SU_PASSWORD}"
gunicorn -b "0.0.0.0:8000" saveapi.wsgi
