import json

from django.conf import settings
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import override_settings
from rest_framework.test import RequestsClient
from requests.auth import HTTPBasicAuth

from saveapi.test_utils import TestCase
from saveapi.models import Link


class APIHelper:
    @staticmethod
    def reverse(viewname, *args, **kwargs):
        reverse_lookup = reverse(viewname, *args, **kwargs)
        return f'https://localhost{reverse_lookup}'


class RegistrationTest(TestCase):
    def setUp(self):
        super().setUp()

        self.client = RequestsClient()

    def test_should_create_user(self):
        registration_json = {
            'uname': 'api tester',
            'pass': 'sup3rs3cur3'
        }
        headers = {
            'Content-Type': 'application/json'
        }

        api_response = self.client.post(
            APIHelper.reverse('register'),
            data=json.dumps(registration_json),
            headers=headers
        )

        self.assertEqual(api_response.status_code, 201)
        self.assertEqual(get_user_model().objects.get(username='api tester').username, 'api tester')

    def test_should_not_create_user_when_username_taken(self):
        registration_json = {
            'uname': 'api tester',
            'pass': 'sup3rs3cur3'
        }
        headers = {
            'Content-Type': 'application/json'
        }
        self.create_test_user('api tester')

        api_response = self.client.post(
            APIHelper.reverse('register'),
            data=json.dumps(registration_json),
            headers=headers
        )

        self.assertEqual(api_response.status_code, 409)

    def test_should_return_error_code_for_invalid_credentials(self):
        registration_json = {
            'uname': 'where is my password'
        }
        headers = {
            'Content-Type': 'application/json'
        }
        self.create_test_user('api tester')

        api_response = self.client.post(
            APIHelper.reverse('register'),
            data=json.dumps(registration_json),
            headers=headers
        )

        self.assertEqual(api_response.status_code, 400)

    @override_settings(ALLOW_REGISTRATION=False)
    def test_should_not_create_user_when_registration_disabled(self):
        settings.ALLOW_REGISTRATION = False
        registration_json = {
            'uname': 'api tester',
            'pass': 'sup3rs3cur3'
        }
        headers = {
            'Content-Type': 'application/json'
        }

        api_response = self.client.post(
            APIHelper.reverse('register'),
            data=json.dumps(registration_json),
            headers=headers
        )

        self.assertEqual(api_response.status_code, 501)


class SaveLinksTest(TestCase):
    def setUp(self):
        super().setUp()

        self.client = RequestsClient()

    def test_should_save_link_from_valid_json_for_authenticated_user(self):
        link_json = {
            'url': 'https://example.com',
            'annotation': 'An annotation'
        }
        headers = {
            'Content-Type': 'application/json'
        }
        user = self.create_test_user()
        self.client.auth = HTTPBasicAuth('tester', 's3cur3')

        api_response = self.client.post(
            APIHelper.reverse('links'),
            data=json.dumps(link_json),
            headers=headers
        )

        self.assertEqual(api_response.status_code, 201)
        link = Link.objects.get(pk=api_response.json()['link']['id'])
        self.assertEqual(link.url, link_json['url'])
        self.assertEqual(link.annotation, link_json['annotation'])

    def test_should_return_error_for_invalid_json(self):
        link_json = {}

        headers = {
            'Content-Type': 'application/json'
        }
        user = self.create_test_user()
        self.client.auth = HTTPBasicAuth('tester', 's3cur3')

        api_response = self.client.post(
            APIHelper.reverse('links'),
            data=json.dumps(link_json),
            headers=headers
        )

        self.assertEqual(api_response.status_code, 400)

    def test_should_return_error_for_trying_to_save_list(self):
        link_json = []

        headers = {
            'Content-Type': 'application/json'
        }
        user = self.create_test_user()
        self.client.auth = HTTPBasicAuth('tester', 's3cur3')

        api_response = self.client.post(
            APIHelper.reverse('links'),
            data=json.dumps(link_json),
            headers=headers
        )

        self.assertEqual(api_response.status_code, 400)

    def test_should_return_error_for_unauthenticated(self):
        link_json = {
            'url': 'https://example.com',
            'annotation': 'An annotation'
        }
        headers = {
            'Content-Type': 'application/json'
        }

        api_response = self.client.post(
            APIHelper.reverse('links'),
            data=json.dumps(link_json),
            headers=headers
        )

        self.assertEqual(api_response.status_code, 401)


class ListLinksTest(TestCase):
    def setUp(self):
        super().setUp()

        self.client = RequestsClient()

    def test_should_return_links_for_user(self):
        user = self.create_test_user()
        user_link_1 = Link.from_json({'url': 'https://example.com'}, user)
        user_link_2 = Link.from_json({'url': 'https://example.com/2'}, user)

        self.client.auth = HTTPBasicAuth('tester', 's3cur3')

        api_response = self.client.get(
            APIHelper.reverse('links')
        )
        expected_link_list = [user_link_1.to_json(), user_link_2.to_json()]

        self.assertEqual(api_response.status_code, 200)
        self.assertEqual(api_response.json(), expected_link_list)

    def test_should_return_empty_list_for_no_links(self):
        user = self.create_test_user()

        self.client.auth = HTTPBasicAuth('tester', 's3cur3')

        api_response = self.client.get(
            APIHelper.reverse('links')
        )

        self.assertEqual(api_response.status_code, 200)
        self.assertEqual(api_response.json(), [])

    def test_should_return_error_for_unauthenticated(self):
        api_response = self.client.get(
            APIHelper.reverse('links')
        )

        self.assertEqual(api_response.status_code, 401)

    def test_should_return_single_link_by_id(self):
        user = self.create_test_user()
        link = Link.from_json({'url': 'https://example.com'}, user)

        self.client.auth = HTTPBasicAuth('tester', 's3cur3')

        api_response = self.client.get(
            APIHelper.reverse('links', kwargs={'link_id': link.id})
        )

        self.assertEqual(api_response.status_code, 200)
        self.assertEqual(api_response.json(), link.to_json())

    def test_should_return_404_for_nonexisting_link(self):
        user = self.create_test_user()

        self.client.auth = HTTPBasicAuth('tester', 's3cur3')

        api_response = self.client.get(
            APIHelper.reverse('links', kwargs={'link_id': 42})
        )

        self.assertEqual(api_response.status_code, 404)


class DeleteLinksTest(TestCase):
    def setUp(self):
        super().setUp()

        self.client = RequestsClient()

    def test_should_delete_link(self):
        user = self.create_test_user()
        link = Link.from_json({'url': 'https://example.com'}, user)
        self.client.auth = HTTPBasicAuth('tester', 's3cur3')

        api_response = self.client.delete(
            APIHelper.reverse('links', kwargs={'link_id': link.id})
        )

        self.assertEqual(api_response.status_code, 200)

    def test_should_return_404_for_nonexisting_link(self):
        user = self.create_test_user()
        self.client.auth = HTTPBasicAuth('tester', 's3cur3')

        api_response = self.client.delete(
            APIHelper.reverse('links', kwargs={'link_id': 42})
        )

        self.assertEqual(api_response.status_code, 404)

    def test_should_return_404_for_foreign_link(self):
        user = self.create_test_user('tester')
        different_user = self.create_test_user('someone else')
        foreign_link = Link.from_json({'url': 'https://example.com'}, different_user)

        self.client.auth = HTTPBasicAuth('tester', 's3cur3')

        api_response = self.client.delete(
            APIHelper.reverse('links', kwargs={'link_id': foreign_link.id})
        )

        self.assertEqual(api_response.status_code, 404)
