from django.contrib.auth import get_user_model
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError

from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView

import logging
logger = logging.getLogger(__name__)

from saveapi.models import Link


class Links(APIView):
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]
    parser_classes = [JSONParser]

    def post(self, request):
        logger.debug(f'Got link data: {request.data}')

        try:
            link = Link.from_json(request.data, request.user)
        except ValidationError as e:
            logger.error(e)
            response_content = {
                'error': 'Link json invalid.'
            }
            response_code = 400
        except IntegrityError as e:
            logger.error(e)
            response_content = {
                'error': 'Link already exists.'
            }
            response_code = 400
        except AttributeError as e:
            logger.error(e)
            response_content = {
                'error': 'Please save multiple links individually.'
            }
            response_code = 400
        else:
            response_content = {
                'link': link.to_json(),
                'success': 'Link saved.'
            }
            response_code = 201

        return Response(response_content, response_code)

    def get(self, request, link_id=None):
        if link_id:
            link = request.user.link(link_id)
            return Response(link.to_json(), 200)
        else:
            sort_order = request.GET.get('sort', 'asc')
            user_links = request.user.links(sort_order)
            user_links_json_list = [link.to_json() for link in user_links]
            return Response(user_links_json_list, 200)

    def delete(self, request, link_id):
        link = request.user.link(link_id)
        link.delete()

        return Response({}, 200)


class Register(APIView):
    parser_classes = [JSONParser]

    def post(self, request):
        if not settings.ALLOW_REGISTRATION:
            response_content = {
                'error': 'Registration is currently disabled.'
            }
            return Response(response_content, 501)


        try:
            username = request.data['uname']
            password = request.data['pass']
        except KeyError as e:
            response_content = {
                'error': 'Username and password required.'
            }
            return Response(response_content, 400)

        try:
            logger.debug(f'Trying to create new user: {username}')
            new_user = get_user_model().objects.create_user(username=username, password=password)
        except IntegrityError as e:
            logger.error(e)
            response_content = {
                'error': 'Username already taken.'
            }
            return Response(response_content, 409)

        response_content = {
            'new_user': new_user.id,
            'success': 'User created.'
        }
        return Response(response_content, 201)
