from django.conf.urls import include
from django.urls import path, re_path

from . import views

urlpatterns = [
    re_path('links/(?P<link_id>[0-9]+)', views.Links.as_view(), name='links'),
    re_path('links', views.Links.as_view(), name='links'),
    path('register', views.Register.as_view(), name='register'),
]
